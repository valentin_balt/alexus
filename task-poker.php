<?php

include __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

$Poker = new Poker();
$Output = new Output();

$Output->newLine();
for ($i = 0; $i < 10; $i++) {
    $numbers = $Poker->getNumbers();
    $Output->text('Throw dices: ' . implode(',', $numbers) . ' -> ');
    $Output->text($Poker->getResult($numbers));
    $Output->newLine();
}