<?php


class Poker
{
    const
        R_POKER = 'poker',
        R_CARE = 'care',
        R_FULL_HOUSE = 'full house',
        R_BIG_STREET = 'big street',
        R_STREET = 'street',
        R_SET = 'set',
        R_TWO_PAIRS = 'two pairs',
        R_PAIR = 'pair',
        R_CHANCE = 'chance',
        R_EMPTY = 'no result';

    public function __construct()
    {

    }

    /**
     * Create and return back 6 numbers from dice
     * @return array
     */
    public function getNumbers(): array
    {
        return (new Dice())->throwDices(5)->getResults();
    }

    /**
     * Return human readable result based on Poker
     * @param array $Numbers Array of 5 numbers from 1 to 6
     * @return string
     */
    public function getResult(array $Numbers): string
    {
        // Count Same numbers
        $CountNumbers = [];
        foreach ($Numbers as $number) {
            if (!isset($CountNumbers[$number])) {
                $CountNumbers[$number] = 0;
            }
            $CountNumbers[$number]++;
        }
        $CountNumbers = array_values($CountNumbers);

        // Detect Sequence
        sort($Numbers, SORT_NUMERIC);
        $sequences = [];
        $sequence = 1;
        for ($i = 0; $i < 4; $i++) {
            if ($Numbers[$i + 1] - $Numbers[$i] == 1) {
                $sequence++;
            } else {
                $sequences[] = $sequence;
                $sequence = 1;
            }
        }
        $sequences[] = $sequence;

        // Detect Pairs
        $pairs = 0;
        foreach ($CountNumbers as $count) {
            if ($count == 2) {
                $pairs++;
            }
        }

        // Let's find result

        // POKER: 2, 2, 2, 2, 2
        if (count($CountNumbers) == 1) {
            return self::R_POKER;
        }

        // CARE: 2, 2, 2, 2
        if (in_array(4, $CountNumbers)) {
            return self::R_CARE;
        }

        // FULL HOUSE: 2, 2, 3, 3, 3
        if (
            count($CountNumbers) == 2
            && (
                ($CountNumbers[0] == 2 && $CountNumbers[1] == 3)
                ||
                ($CountNumbers[0] == 3 && $CountNumbers[1] == 2)
            )
        ) {
            return self::R_FULL_HOUSE;
        }

        // BIG STREET: 1, 2, 3, 4, 5
        if (in_array(5, $sequences)) {
            return self::R_BIG_STREET;
        }

        // STREET: 1, 2, 3, 4
        if (in_array(4, $sequences)) {
            return self::R_STREET;
        }

        // SET: 1, 1, 1
        if (in_array(3, $CountNumbers)) {
            return self::R_SET;
        }

        // TWO PAIRS: 2, 2, 3, 3
        if ($pairs == 2) {
            return self::R_TWO_PAIRS;
        }

        // ONE PAIR: 2, 2
        if ($pairs == 1) {
            return self::R_PAIR;
        }

        // CHANCE
        if (count($CountNumbers) == 5) {
            return self::R_CHANCE;
        }

        return self::R_EMPTY;
    }
}