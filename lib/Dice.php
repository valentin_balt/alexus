<?php


class Dice
{
    private int $DiceSize=6;
    private array $Result;

    public function __construct($DiceSizeInit=6)
    {
        $this->setDiceSize($DiceSizeInit);
    }

    /**
     * Return sum af all thrown dices
     * @return int
     */
    public function getResultSum(): int
    {
        return array_sum($this->Result);
    }

    /**
     * Return all dices as array
     * @return array
     */
    public function getResults(): array
    {
        return $this->Result;
    }

    /**
     * Throw one dice and return the number
     * @return int
     */
    public function throwDice(): int
    {
        return rand(1, $this->getDiceSize());
    }

    /**
     * Throw many dices
     * @param int $amount How many dices to throw
     * @return Dice
     */
    public function throwDices(int $amount): Dice
    {
        $this->Result = [];
        for($i=0;$i<$amount;$i++) {
            $this->Result[] = rand(1, $this->getDiceSize());
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getDiceSize(): int
    {
        return $this->DiceSize;
    }

    /**
     * @param int $DiceSize
     */
    public function setDiceSize(int $DiceSize): Dice
    {
        $this->DiceSize = $DiceSize;
        return $this;
    }

}