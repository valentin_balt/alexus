<?php


class Output
{
    public function __construct()
    {

    }

    protected function is_cli(): bool
    {
        return php_sapi_name() === 'cli';
    }

    public function text(string $String): Output
    {
        echo $String;
        return $this;
    }

    public function newLine(): Output
    {
        echo $this->is_cli() ? "\n" : "<br />\n";
        return $this;
    }

}