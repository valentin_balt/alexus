/* Create table */
create table transactions
(
    id     int     not null,
    data   int     not null,
    amount decimal not null
);

/* Insert test data */
insert into transactions (id, data, amount)
values (1, 1, 1),
       (2, 2, 2),
       (6, 3, -1),
       (8, 4, 2),
       (7, 5, -6),
       (9, 6, 1),
       (3, 7, -5),
       (4, 8, 1),
       (5, 9, 7);

/* Find first transaction which turns balance negative*/
select t1.id, sum(t2.amount) as amnt
from transactions t1
         join transactions t2 on t1.data >= t2.data
group by t1.data
having amnt < 0
limit 1;