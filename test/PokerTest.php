<?php
include dirname(__DIR__) . DIRECTORY_SEPARATOR . 'bootstrap.php';

use PHPUnit\Framework\TestCase;

class PokerTest extends TestCase
{
    public function testNumbers()
    {
        $Poker = new Poker();
        $numbers = $Poker->getNumbers();

        $this->assertIsArray($numbers);
        $this->assertCount(5, $numbers);

        foreach ($numbers as $number) {
            $this->assertIsInt($number);
        }
    }

    public function testPoker()
    {
        $Poker = new Poker();
        $result = $Poker->getResult([1, 1, 1, 1, 1]);

        $this->assertEquals(Poker::R_POKER, $result);
    }

    public function testCare()
    {
        $Poker = new Poker();
        $result = $Poker->getResult([1, 1, 1, 1, 2]);

        $this->assertEquals(Poker::R_CARE, $result);
    }

    public function testFullHouse()
    {
        $Poker = new Poker();
        $result = $Poker->getResult([1, 1, 2, 2, 2]);

        $this->assertEquals(Poker::R_FULL_HOUSE, $result);
    }

    public function testBigStreet()
    {
        $Poker = new Poker();
        $result = $Poker->getResult([1, 2, 3, 4, 5]);

        $this->assertEquals(Poker::R_BIG_STREET, $result);
    }

    public function testStreet()
    {
        $Poker = new Poker();
        $result = $Poker->getResult([1, 2, 3, 4, 6]);

        $this->assertEquals(Poker::R_STREET, $result);
    }

    public function testSet()
    {
        $Poker = new Poker();
        $result = $Poker->getResult([1, 1, 1, 4, 6]);

        $this->assertEquals(Poker::R_SET, $result);
    }

    public function testTwoPairs()
    {
        $Poker = new Poker();
        $result = $Poker->getResult([1, 1, 2, 2, 6]);

        $this->assertEquals(Poker::R_TWO_PAIRS, $result);
    }

    public function testPair()
    {
        $Poker = new Poker();
        $result = $Poker->getResult([1, 1, 2, 3, 5]);

        $this->assertEquals(Poker::R_PAIR, $result);
    }

    public function testChance()
    {
        $Poker = new Poker();
        $result = $Poker->getResult([1, 2, 4, 5, 6]);

        $this->assertEquals(Poker::R_CHANCE, $result);
    }
}
