<?php
include dirname(__DIR__) . DIRECTORY_SEPARATOR . 'bootstrap.php';

use PHPUnit\Framework\TestCase;

class DiceTest extends TestCase
{
    public function testDice()
    {
        $diceNumber = (new Dice())->throwDice();

        $this->assertIsInt($diceNumber);

        $dices = (new Dice())->throwDices(3)->getResults();
        $this->assertCount(3, $dices);

        $dicesSum = (new Dice())->throwDices(3)->getResultSum();
        $this->assertIsInt($dicesSum);
    }
}
